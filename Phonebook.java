package com.zuitt.activity;

import java.util.ArrayList;

public class Phonebook {

    private ArrayList<Contact> contacts = new ArrayList<Contact>();

    //default constructor
    public Phonebook(){}

    //parameterized constructor
    public Phonebook(Contact contact){
        this.contacts.add(contact);
    }


    //getter
    public ArrayList<Contact> getContacts(){return this.contacts = contacts;}


    //setter
    //public void setContacts(ArrayList<Contact> contacts){ this.contacts = contacts;}
    /*public void setContacts(Contact contacts){
        this.contacts = contacts;
    }*/
    public void setContacts(Contact contact) {
        this.contacts.add(contact);
    }
}
